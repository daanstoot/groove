package groove.network;

import groove.util.cli.GrooveCmdLineTool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class GrooveChannel extends GrooveCmdLineTool<Object> {

	public static final boolean DEBUG = false;
	private final int port;
	private volatile boolean running = true;
	private final List<NetworkHelper> helpers = new ArrayList<>();

	public GrooveChannel(String[] args) {
		// TODO parse arguments
		super("NetworkApp", args);
		this.port = 8099;

	}

	static public void main(String[] args) {
		GrooveCmdLineTool.tryExecute(GrooveChannel.class, args);
	}

	/**
	 * Starts a ServerSocket, listening to incoming connections
	 *
	 * @param args GrooveChannel options and arguments
	 * @return null
	 */
	static public Object execute(String... args) throws Exception {
		GrooveChannel grooveChannel = new GrooveChannel(args);
		grooveChannel.start();
		return null;
	}

	@Override
	protected Object run() throws Exception {
		ServerSocket servSock = new ServerSocket(port);

		try {
			if (DEBUG) {
				BufferedWriter sysOut = new BufferedWriter(new OutputStreamWriter(System.out));
				BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
				NetworkHelper helper = new NetworkHelper(sysIn, sysOut, null, this);
				helper.start();
				addHelper(helper);
			}
			while (!servSock.isClosed() && running) {
				Socket sock = servSock.accept();
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
				BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
				NetworkHelper helper = new NetworkHelper(in, out, sock, this);
				helper.start();
				addHelper(helper);
			}
		} finally {
			// Damn
			running = false;
			interruptHelpers();

		}
		return null;
	}

	private synchronized void interruptHelpers() {
		helpers.forEach(Thread::interrupt);
	}

	private synchronized void addHelper(NetworkHelper helper) {
		helpers.add(helper);
	}

	public synchronized void removeHelper(NetworkHelper networkHelper) {
		helpers.remove(networkHelper);
		if (!DEBUG)
			System.out.println("Removed: " + networkHelper);
	}
}
