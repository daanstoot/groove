package groove.network.views;

import java.util.List;

public class StateView {

	private final List<TransitionView> transitions;
	private final List<DialogTransitionView> dialogTransitions;

	public StateView(List<TransitionView> transitions, List<DialogTransitionView> dialogTransitions) {
		this.transitions = transitions;
		this.dialogTransitions = dialogTransitions;
	}

	public List<TransitionView> getTransitions() {
		return transitions;
	}

	public List<DialogTransitionView> getDialogTransitions() {
		return dialogTransitions;
	}
}
