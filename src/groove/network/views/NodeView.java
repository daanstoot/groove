package groove.network.views;

import com.fasterxml.jackson.annotation.JsonInclude;
import groove.grammar.host.HostNode;
import groove.grammar.host.ValueNode;

public class NodeView {
	private final int number;
	private final String type;
	private final Object value;

	public NodeView(HostNode n) {
		this.number = n.getNumber();
		this.type = n.getType().toString();
		if (n instanceof ValueNode)
			this.value = ((ValueNode) n).getValue();
		else
			this.value = null;

	}

	public int getNumber() {
		return number;
	}

	public String getType() {
		return type;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public Object getValue() {
		return value;
	}
}
