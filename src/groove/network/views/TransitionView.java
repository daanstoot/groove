package groove.network.views;

import groove.lts.GraphTransition;

public class TransitionView {
	private final int source;
	private final String name;
	private final int target;

	public TransitionView(GraphTransition transition) {
		this.source = transition.source().getNumber();
		this.name = transition.text(false);
		this.target = transition.target().getNumber();
	}

	public int getSource() {
		return source;
	}

	public String getName() {
		return name;
	}

	public int getTarget() {
		return target;
	}
}
