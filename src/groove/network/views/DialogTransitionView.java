package groove.network.views;

import groove.lts.MatchResult;

public class DialogTransitionView {

	private final String name;

	public DialogTransitionView(MatchResult result) {
		this.name = result.getEvent().getRule().getQualName().toString();
	}

	public String getName() {
		return name;
	}
}
