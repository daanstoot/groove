package groove.network.views;

import java.util.List;

public class GraphView {

	private final List<NodeView> nodes;
	private final List<EdgeView> edges;

	public GraphView(List<NodeView> nodes, List<EdgeView> edges) {
		this.nodes = nodes;
		this.edges = edges;
	}

	public List<NodeView> getNodes() {
		return nodes;
	}

	public List<EdgeView> getEdges() {
		return edges;
	}
}
