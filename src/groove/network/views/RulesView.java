package groove.network.views;

import groove.grammar.Rule;
import groove.grammar.Signature;
import groove.grammar.UnitPar;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RulesView {

	private final Map<String, RuleView> rules;

	public RulesView(Set<Rule> rules) {
		this.rules = rules.stream().collect(Collectors.toMap(Rule::getTransitionLabel, RuleView::new, (v1, v2) -> v2, LinkedHashMap::new));

	}

	public Map<String, RuleView> getRules() {
		return rules;
	}

	public static class RuleView {
		private final String name;
		private final SignatureView signature;

		public RuleView(Rule rule) {
			this.name = rule.getTransitionLabel();
			this.signature = new SignatureView(rule.getSignature());
		}

		public String getName() {
			return name;
		}

		public SignatureView getSignature() {
			return signature;
		}
	}

	public static class SignatureView {
		private final List<ParameterView> pars;

		public SignatureView(Signature<UnitPar.RulePar> signature) {
			this.pars = signature.getPars().stream().map(ParameterView::new).collect(Collectors.toList());
		}

		public List<ParameterView> getPars() {
			return pars;
		}
	}

	public static class ParameterView {
		private final String name;
		private final String type;

		public ParameterView(UnitPar.RulePar par) {
			this.name = par.getName();
			this.type = par.getType().toString();
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}
	}
}
