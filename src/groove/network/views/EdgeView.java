package groove.network.views;

import groove.grammar.host.HostEdge;

public class EdgeView {
	private final int number;
	private final String type;
	private final String label;
	private final int source;
	private final int target;

	public EdgeView(HostEdge e) {
		this.number = e.getNumber();
		this.type = e.getType().toString();
		this.label = e.label().toString();
		this.source = e.source().getNumber();
		this.target = e.target().getNumber();
	}

	public int getNumber() {
		return number;
	}

	public String getType() {
		return type;
	}

	public String getLabel() {
		return label;
	}

	public int getSource() {
		return source;
	}

	public int getTarget() {
		return target;
	}
}
